import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Redirect,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @Redirect('health', HttpStatus.MOVED_PERMANENTLY)
  root() {
    return;
  }
  @HttpCode(200)
  @Get('health')
  async getHealth(): Promise<{ status: number; message: string }> {
    return { status: HttpStatus.OK, message: 'test.API.ONLINE' };
  }
}
