import { JWT_PAYLOAD, UserROLES } from './../types/index';
import {
  ExecutionContext,
  SetMetadata,
  createParamDecorator,
} from '@nestjs/common';

export const ROLES_KEY = 'role';
export const ROLESACCESS = (...role: UserROLES[]) =>
  SetMetadata(ROLES_KEY, role);

export const User = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const user: JWT_PAYLOAD = request.user;
    return data ? user?.[data] : user;
  },
);
