import { IUser } from '../entities';
import { UserROLES, UserSTATUS } from '../types';
import {
  IsDate,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsPhoneNumber,
  IsString,
} from 'class-validator';
import { PickType } from '@nestjs/mapped-types';

export class UserDto implements IUser {
  @IsString()
  @IsNotEmpty()
  fullName: string;

  @IsEmail()
  @IsNotEmpty()
  @IsString()
  email: string;

  @IsString()
  @IsPhoneNumber()
  @IsNotEmpty()
  phoneNumber: string;

  @IsString()
  @IsNotEmpty()
  @IsEnum(UserROLES)
  roles: UserROLES;

  @IsString()
  @IsNotEmpty()
  @IsEnum(UserSTATUS)
  status: UserSTATUS;

  @IsDate()
  @IsNotEmpty()
  createdAt: Date;

  @IsDate()
  @IsNotEmpty()
  updatedAt: Date;
}

export class CreateUserDto extends PickType(UserDto, [
  'fullName',
  'email',
  'phoneNumber',
] as const) {}
