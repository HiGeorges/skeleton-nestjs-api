import { UserROLES, UserSTATUS } from '../types';

export interface IUser {
  email: string;
  fullName: string;

  phoneNumber: string;

  roles: UserROLES;
  status: UserSTATUS;

  createdAt: Date;
  updatedAt: Date;
}

export type UserDocument = IUser & Document;
