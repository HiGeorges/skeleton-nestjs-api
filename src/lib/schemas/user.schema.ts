import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { UserROLES, UserSTATUS } from '../types';
import { IUser } from '../entities';

@Schema()
export class User implements IUser {
  @Prop({ required: true })
  fullName: string;

  @Prop({ unique: true, required: true })
  email: string;

  @Prop({ required: true, unique: true })
  phoneNumber: string;

  @Prop({
    enum: UserSTATUS,
    default: UserSTATUS.CREATED,
  })
  status: UserSTATUS;

  @Prop({
    enum: UserROLES,
    default: UserROLES.USER,
  })
  roles: UserROLES;

  @Prop({ default: () => new Date() })
  createdAt: Date;

  @Prop({ default: () => new Date() })
  updatedAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
