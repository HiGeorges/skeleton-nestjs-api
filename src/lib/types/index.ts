export enum UserSTATUS {
  CREATED = 'CREATED',
  SUSPENDED = 'SUSPENDED',
  DISABLED = 'DISABLED',
  VERIFIED = 'VERIFIED',
}

export enum UserROLES {
  ADMIN = 'ADMIN',
  USER = 'USER',
}

export type JWT_PAYLOAD = {
  _id: string;
  roles: UserROLES;
  status: UserSTATUS;
  email: string;
};
