import { HttpException, HttpStatus } from '@nestjs/common';
import { generate } from 'otp-generator';
import { v4, validate } from 'uuid';

export const generateOtp = (length: number): string => {
  return generate(length, {
    digits: true,
    lowerCaseAlphabets: false,
    upperCaseAlphabets: true,
    specialChars: false,
  });
};

export const generateUUID = (): string => v4();
export const verifyUUID = (uuid: string): boolean => validate(uuid);

export const isOtpExpired = (createdAt: Date, ttl: number): boolean => {
  // check if otp is expired
  const now = new Date().getTime();
  const diff = now - createdAt.getTime();
  return diff > ttl * 60 * 1000; // ttl in minutes
};
